<link rel="stylesheet" type="text/css" href="7.css">
<!DOCTYPE html>
<html>
<head>
	<title>Product Arkademy</title>
</head>
<body>
	<div class="bayangan">
	<h1>PRODUCT ARKADEMY</h1>
	<?php
	$host       =   "localhost";
	$user       =   "root";
	$password   =   "";
	$database   =   "product";

	$conn = mysqli_connect($host,$user,$password,$database);
	if (!$conn) {
		die ('Gagal terhubung MySQL: ' . mysqli_connect_error());	
	}
	$sql = 'SELECT * FROM product_categories';
	$query = mysqli_query($conn, $sql);

	if (!$query) {
		die ('SQL Error: ' . mysqli_error($conn));
	}
	echo '
		<table border="1" style="border-collapse: collapse;">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Jumlah Produk</th>
				</tr>
			</thead>
		</tbody>
	';

	while ($row = mysqli_fetch_array($query))
	{
		echo '<tr>
				<td>'.$row['categories_id'].'</td>
				<td>'.$row['name'].'</td>
				<td>'.$row['jumlah_produk'].'</td>
			</tr>';
	}
	echo '
		</tbody>
	</table>';

mysqli_close($conn);

?>
</div>
</body>
</html>
